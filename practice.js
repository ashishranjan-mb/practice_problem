const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}



/*Q1. Find all the movies with total earnings more than $500M.
Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
Q.3 Find all movies of the actor "Leonardo Dicaprio".
Q.4 Sort movies (based on IMDB rating)
    if IMDB ratings are same..compare totalEarning.
Q.5 Group movies based on genre. Priority of genre in case of multiple genres present
    drama > sci-fi > adventure > thriller > crime

*/


//PROBLEM 1
let arr = [];
Object.entries(favouritesMovies).forEach(element => {
    let data = element[1]
    if (data.totalEarnings.replace('$','').replace("M",'')*1 > 500)
    {
        arr.push(element)
    }  
});

console.log(arr)

//PROBLEM 2
let arr1 = [];
Object.entries(favouritesMovies).forEach(element => {
    let data = element[1]
    if (data.totalEarnings.replace('$','').replace("M",'')*1 > 500 && data.oscarNominations > 3)
    {
        arr1.push(element)
    }  
});

console.log(arr1)

//PROBLEM 3
let arr = [];
Object.entries(favouritesMovies).forEach(element => {
    let data = element[1]
    if (data.actors.includes("Leonardo Dicaprio"))
    {
        arr.push(element)
    }  
});

console.log(arr)

//PROBLEM 4

let arr = Object.entries(favouritesMovies);
console.log(arr.sort(sortFunction));
function sortFunction(a, b) {
    if (a[1].imdbRating === b[1].imdbRating) {
        return (a[1].totalEarnings < b[1].totalEarnings) ? -1 : 1;
    }
    else {
        return (a[1].imdbRating < b[1].imdbRating) ? -1 : 1;
    }
}

//PROBLEM 5

 
let res = Object.entries(favouritesMovies).reduce((acc, curr) => {
    let genreArray=curr[1].genre;
    if(genreArray.includes('drama')){
        if(!('drama' in acc))
            acc['drama'] = [];
        acc['drama'].push(curr[0]);
    }
    else if(genreArray.includes('sci-fi')){
        if(!('sci-fi' in acc))
            acc['sci-fi'] = [];
        acc['sci-fi'].push(curr[0]);
    }
    else if(genreArray.includes('adventure')){
        if(!('adventure' in acc))
            acc['adventure'] = [];
        acc['adventure'].push(curr[0]);
    }
    else if(genreArray.includes('thriller')){
        if(!('thriller' in acc))
            acc['thriller'] = [];
        acc['thriller'].push(curr[0]);
    }
    else if(genreArray.includes('crime')){
        if(!('crime' in acc))
            acc['crime'] = [];
        acc['crime'].push(curr[0]);
    }
    return acc;
}, {})
console.log(res);

